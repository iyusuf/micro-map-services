package com.sortedset.blog.embeddedjava.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.sortedset.blog.embeddedjava.spring")
class AppConfig {
}