package com.sortedset.blog.embeddedjava.spring;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class RestGisController {

	private static Logger LOGGER = Logger.getLogger(RestGisController.class);
   
   //http://localhost:20577/webapp/spring/countries/border?Name=Canada
   
    @RequestMapping("/countries/border")
    public String countries(@RequestParam(value="Name", defaultValue="United States") String name) {
    	LOGGER.info("Request parameter : " + name);
		RestGisResource r =  new RestGisResource();
		try {
			String result = r.getCountryBorder(name);
			if(result.isEmpty()){
				return "{\"servermsg\" : \"Query for country = "+ name + " came out empty \"}" ;
			}else {
				LOGGER.info("\n result \n");
				LOGGER.info(result);
				LOGGER.info("\n\n result end \n");
				return result;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return "{\"servermsg\" : \"Server Error\"}";
		}
		

    }
	
	
	@RequestMapping("/countries/names")
    public String countryNames() {

		RestGisResource r =  new RestGisResource();
		try {
			String result = r.getCountryNames();
			if(result.isEmpty()){
				return "{\"servermsg\" : \"Query for country names came out empty \"}" ;
			}else {
				return result;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return "{\"servermsg\" : \"Server Error\"}";
		}
		

    }
    

}