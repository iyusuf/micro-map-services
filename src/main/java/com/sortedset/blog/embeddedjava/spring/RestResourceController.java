package com.sortedset.blog.embeddedjava.spring;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestResourceController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    
    @RequestMapping("/hello")
    public String hello() {
    	return "hello from Spring greeting";
    }
    
    
    
    @RequestMapping("/greeting")
    public RestResource greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new RestResource(counter.incrementAndGet(),
                            String.format(template, name));
    }
    

}