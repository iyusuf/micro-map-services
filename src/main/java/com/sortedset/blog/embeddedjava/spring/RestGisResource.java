package com.sortedset.blog.embeddedjava.spring;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class RestGisResource {


	public RestGisResource(){

	}


	public String getCountryBorder(String countryName) throws Exception {

		Class.forName("org.h2.Driver");
		/**
		 * To start the console to manage the database use the following from command line. Match the location of db file to your location.
		 C:\servers\h2gis-standalone>java -cp h2-dist-1.1.0.jar org.h2.tools.Console -url jdbc:h2:file:C:\projects\java_projects\maven-tomcat-embedded-jersey-spring\micro-map-services\data\testgis
		
		 */
		Connection conn = DriverManager.getConnection("jdbc:h2:file:data/testgis");
		Statement stat = conn.createStatement();

		StringBuilder sb = new StringBuilder();

		ResultSet rs;
		//rs = stat.executeQuery("select (name_1)  from usa_states ;");
		rs = stat.executeQuery("select ST_AsGeoJSON(the_geom) , name  from countries where name='"+countryName+"'");
		//rs = stat.executeQuery("select (the_geom) from usa_states limit 1");
		while (rs.next()) {
			//System.out.println(rs.getString(1));
			//System.out.println("\n" +rs.getRow());
			sb.append(" { \"type\": \"FeatureCollection\", \"features\": [{ \"type\": \"Feature\",\"properties\": {\"name\": \""+ countryName +"\"},\"geometry\": ");
			sb.append(rs.getString(1) );
			sb.append("}]}");

		}
		rs.close();
		stat.close();
		conn.close();

		return sb.toString();
	}
	
	public String getCountryNames() throws Exception {

		Class.forName("org.h2.Driver");
		/**
		 * To start the console to manage the database use the following from command line. Match the location of db file to your location.
		 C:\servers\h2gis-standalone>java -cp h2-dist-1.1.0.jar org.h2.tools.Console -url jdbc:h2:file:C:\projects\java_projects\maven-tomcat-embedded-jersey-spring\micro-map-services\data\testgis
		
		 */
		Connection conn = DriverManager.getConnection("jdbc:h2:file:data/testgis");
		Statement stat = conn.createStatement();

		StringBuilder sb = new StringBuilder();

		ResultSet rs;
		rs = stat.executeQuery("select name from countries");

		while (rs.next()) {
			System.out.println(rs.getString(1));
			System.out.println("\n" +rs.getRow());
			sb.append(rs.getString(1) + ",");

		}
		rs.close();
		stat.close();
		conn.close();

		return sb.toString();
	}

	public static void main(String[] args)  {
		
		try {
			String result = new RestGisResource().getCountryBorder("United Statesd");
			if (result.isEmpty()){
				System.out.println("Emptry query result");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
	}



}