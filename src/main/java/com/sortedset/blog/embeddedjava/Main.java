package com.sortedset.blog.embeddedjava;

import java.io.File;

import org.apache.catalina.Context;
import org.apache.catalina.WebResourceRoot;
//to make @WebServlet work
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.apache.log4j.Logger;

import com.sortedset.blog.embeddedjava.servlets.HelloWorldServlet;


public class Main 
{
private static Logger LOGGER = Logger.getLogger(Main.class);

    public static void main( String[] args ) throws Exception
    {
	
		String webappDirLocation = "webapp/";
        Tomcat tomcat = new Tomcat();

        //The port that we should run on can be set into an environment variable
        //Look for that variable and default to 8080 if it isn't there.
        String webPort = System.getenv("PORT");
        if(webPort == null || webPort.isEmpty()) {
            webPort = "20577";
        }

        tomcat.setPort(Integer.valueOf(webPort));
        
		// Add web application
        tomcat.addWebapp("/webapp", new File(webappDirLocation).getAbsolutePath());
        LOGGER.info("configuring app with basedir: " + new File(webappDirLocation).getAbsolutePath());
		

		// Servlet 
		// that got added from this class
		File base = new File(webappDirLocation);
		Context rootCtx = tomcat.addContext("/servlets", base.getAbsolutePath());
		Tomcat.addServlet(rootCtx, "helloWorldServlet", new HelloWorldServlet());
		rootCtx.addServletMapping("/helloworld", "helloWorldServlet");
		
  
		//Needed code to make @WebServlet annotation work: 
		String contextPath="/servlets2";
		String baseDirectory = new File(webappDirLocation).getAbsolutePath();
		StandardContext context = (StandardContext) tomcat.addWebapp(contextPath, baseDirectory);
		File additionWebInfClasses = new File("target/classes");
		WebResourceRoot resources = new StandardRoot(context);
		resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes", additionWebInfClasses.getAbsolutePath(), "/"));
		context.setResources(resources);

        tomcat.start();
        tomcat.getServer().await();
		
    }
}
